
PATH := /projects/
FILE := .

inLCompile:
	pdflatex --output-directory=./out -shell-escape Tesi.tex
	bibtex out/Tesi
	pdflatex --output-directory=./out -shell-escape Tesi.tex

outLCompile: 
	docker exec -it latexr /bin/bash -c "cd $(PATH)$(FILE) && pdflatex --output-directory=./out -shell-escape Tesi.tex"
	docker exec -it latexr /bin/bash -c "cd $(PATH)$(FILE) && bibtex ./out/Tesi"
	docker exec -it latexr /bin/bash -c "cd $(PATH)$(FILE) && pdflatex --output-directory=./out -shell-escape Tesi.tex"

clean:
	rm ./out/*






# .PHONY: all clean

# all: main.pdf

# main.pdf: main.tex references.bib
# 	pdflatex $(TEXFILE)
# 	bibtex $(TEXFILE:.tex=.bib)
# 	pdflatex $(TEXFILE)
# 	pdflatex $(TEXFILE)
# 	pdflatex $(TEXFILE)
# 	pdflatex $(TEXFILE)

# clean:
# 	rm -f main.pdf *.aux *.log *.blg *.bbl *.out
